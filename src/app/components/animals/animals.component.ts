import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.scss']
})
export class AnimalsComponent implements OnInit {
  //Prorperties 
  zooTitle = "East London Zoo"
  animals = ["Leopard ", "Lynx", "Capybara", "Shark"]
  zooSite = "https://a-z-animals.com/animals/"

  //static joinWithSlash: (start: string, end: string) => string
  
  constructor() { }

  ngOnInit(): void {
  }

}
